## What is git-poly ?

A tool to help developing across 50+ git repos easier. It's a simple,
light weight tool to parallelize working with many git repos.

## What language ?
git-poly is written in rust.

### The Goal
Make working with many git repos feel like working in a single git repo for
most day to day operations, while still allowing those repos to be
completely seperate.

### Features
- Very fast!
- Almost all operations are done in parallel
- Multi-platform (mac, linux, windows)
- Searching for git repos is done asynchronously, the moment we have found a git
  repo a new thread is created to process it.
- Regex based find and replace
- There's no config/manifest file.
- Aligns very closely with git, only four more very straight forward commands
are added.

# Usage
See [here](https://luke_titley.gitlab.io/git-poly) for pre-built binaries and usage instructions.

# Build
Use cargo

```
cargo build
```
